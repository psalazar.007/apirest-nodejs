﻿# Host: localhost  (Version 5.5.5-10.4.24-MariaDB)
# Date: 2023-05-21 19:40:14
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "usuarios"
#

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ci` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `primer_apellido` varchar(255) DEFAULT NULL,
  `segundo_apellido` varchar(255) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

#
# Data for table "usuarios"
#

INSERT INTO `usuarios` VALUES (1,'111111','MARCELO ','LIMACHI','CACERES','1980-05-30'),(2,'222222','CARLOS','SONTURA','QUISBERT','1987-10-15'),(3,'333333','JULIAN','APAZA','MAMANI','1988-10-10'),(4,'444444','AMTONIETA','CANAZA','CHAVEZ','1990-11-10'),(5,'555555','MARIA','CHUQUIMIA','LIMACHI','1988-10-10'),(6,'666666','OLIVIA MODIFICADO','SANCHEZ','SALAS','1987-10-10');
