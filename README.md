# APIREST-NODEJS

## PROYECTO API REST - NODE JS

# API REST
Una API REST es un estilo de arquitectura que se utiliza para diseñar servicios web. Se basa en el principio de "intercambio de representaciones de recursos" y permite que diferentes sistemas se comuniquen entre sí a través de la web utilizando el protocolo HTTP.

Una API REST consta de un conjunto de puntos finales (o "recursos") a los que los clientes pueden enviar solicitudes utilizando distintos métodos HTTP, como GET, POST, PUT y DELETE. 

Para el caso practico gestion de usuarios en la una base de datos. permita realizar las siguientes operaciones:


GET http://localhost:9000/api HTTP/1.1  LISTAR TODOS LOS USUARIOS
POST http://localhost:9000/api HTTP/1.1 REALIZAR UN NUEVO REGISTRO
DELETE http://localhost:9000/api/{id} HTTP/1.1 ELIMINAR UN REGISTRO DE USUARIO
PUT http://localhost:9000/api/{id} HTTP/1.1 EDITAR UN REGISTRO DE USUARIO
GET http://localhost:9000/api/promedio LISTAR PROMEDIO DE EDAD


Cada una de estas operaciones se realiza enviando una solicitud HTTP con el método apropiado (GET, POST, PUT, DELETE) al punto final correspondiente. Los clientes de la API pueden ser otras aplicaciones web, aplicaciones móviles o cualquier otro sistema que necesite acceder a los datos de la base de datos de películas.

## CONFIGURACION API

Debe descargar del repositorio git, la APIREST-NODEJS y clonar en su equipo con los siguientes comandos

git clone https://gitlab.com/psalazar.007/apirest-nodejs.git en una carpeta local de su equipo

## Complementos a instalar

  - Una vez descargado el paquete, debe abrir con un editor "VISUAL STUDIO CODE" en la ruta de la carpeta descargada
  - Debe tener instalado git. 
  - Ingresar a la terminal a travez de visual Studio Code o por linea de comandos "POWER SHELL"
  - Instalar el paquede te NODE_MODULES con el siguiente comando
	- npm install
  - Una vez instalado debe ejecutar iniciar el servidor de aplicaciones de manera local con el siguiente comando:
	- npm run start
   														
## Ejecutar APIREST-NODEJS

  - Para ejecutar request.http debe tener instalado REST CLIENT
  - Puede jejecutar request.http para ejecutar la API o atravez de la URL http://localhost:9000/API

## CONFIGURACION BASE DE DATOS

    - Para la configuracion de la BASE DE DATOS debe tener instalado MYSQL, y luego compliar el archivo bd_registro
    - Actualizar el archivo server.js, donde se encuentra configurdado la conecion a la BD