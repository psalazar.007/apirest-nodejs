const express = require('express')
const routes = express.Router()

// LISTAR TODOS LOS USUARIOS REGISTRADOS-------------------------------------
routes.get('/', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err)

        conn.query('SELECT * FROM usuarios', (err, rows)=>{
            if(err) return res.send(err)

            res.json(rows)
        })
    })
})

// ADICIONAR UN NUEVO REGISTRO DE USUARIOS-------------------------------------
routes.post('/', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err)
        // NOS PERMITE VIZUALIZAR EL REGISTRO ANTES DE INSERTAR
        //console.log(req.body)
        
        // INSERTAMOS EL REGISTRO
        conn.query('INSERT INTO usuarios set ?', [req.body], (err, rows)=>{
          if(err) return res.send(err)
          res.send('Se adicino un registro...!')
        })
    })
})


// EDITAR UN REGISTRO DE USUARIO-------------------------------------
routes.put('/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err)
        conn.query('UPDATE usuarios set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('El registro fue Actualizado...!')
        })
    })
})

// ELIMINAR UN REGISTRO DE USUARIO-------------------------------------
routes.delete('/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err)
        conn.query('DELETE FROM usuarios WHERE id = ?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('El registro fue eliminado...!')
        })
    })
})

// LISTAR PROMEDIO DE EDAD DE LOS USAURIO-------------------------------------
routes.get('/promedio', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err)

        conn.query('SELECT fecha_nacimiento, ROUND(AVG(TIMESTAMPDIFF(YEAR, fecha_nacimiento, CURDATE())),0) Edad_Promedio FROM usuarios HAVING TIMESTAMPDIFF(YEAR, fecha_nacimiento, CURDATE()) > 0', (err, rows)=>{
            if(err) return res.send(err)

            res.json(rows)
        })
    })
})

module.exports = routes