const express = require('express')
const mysql = require('mysql')
const myconn = require('express-myconnection')

const routes = require('./routes')

const app = express()
app.set('port', process.env.PORT || 9000)
const dbOptions = {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'root',
    database: 'bd_registro'
}

// MIDDLEWARES -------------------------------------
app.use(myconn(mysql, dbOptions, 'single'))
app.use(express.json())


// RUTA DE ACCESO INICIAL -------------------------------------------
app.get('/', (req, res)=>{
    res.send('BIENVENIDO NODE JS Y API REST')
})

app.use('/api', routes)

// PORTO DE EJECUCION DEL SERVIDOR -----------------------------------
app.listen(app.get('port'), ()=>{
    console.log('server running on port', app.get('port'))
})